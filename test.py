# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2021 Max-Planck-Society

import os
from collections import defaultdict
from itertools import product

import numpy as np
import pytest

import nifty6 as ift
import src as vlbi
from config import sky_movie_mf


def setup_function():
    import nifty6 as ift
    ift.random.push_sseq_from_seed(42)


def teardown_function():
    import nifty6 as ift
    ift.random.pop_sseq()


pmp = pytest.mark.parametrize
bools = [False, True]
avg_ints = [8]  # FIXME Test without averaging are superslow
d3 = {
    'uv': ift.random.current_rng().random((11, 2))*1e7,
    'vis': ift.random.current_rng().random(11) + 1j*ift.random.current_rng().random(11),
    'sigma': ift.random.current_rng().random(11)**2,
    'freq': 1e11,
    'time': np.array(5*[7.24027777] + 6*[7.4236114]),
    'ant1': np.array([0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 2]),
    'ant2': np.array([1, 2, 3, 2, 3, 1, 2, 3, 2, 3, 3])
}
ds = [vlbi.read_data('data/ehtcrescent', dd, ff, aa) for dd, ff, aa in product(vlbi.DAYS, vlbi.FREQS, avg_ints)]
ds.extend([vlbi.combined_data('data/ehtcrescent', vlbi.FREQS, aa) for aa in avg_ints])
ds.append(d3)

seeds = [189, 4938]
dtypes = [np.complex128, np.float64]
dom = ift.RGSpace(10)


def AntennaBasedCalibration(tspace, time, ant1, ant2, amplkey, phkey):
    assert time.shape == ant1.shape
    assert time.shape == ant2.shape
    assert len(time.shape) == 1
    antspace = ift.UnstructuredDomain(len(set(ant1) | set(ant2)))
    dom = ift.makeDomain((antspace, tspace))
    ddct = defaultdict(lambda: len(ddct))
    ant1 = np.array([ddct[ele] for ele in ant1])
    ant2 = np.array([ddct[ele] for ele in ant2])
    dtr1 = CalibrationDistributor(dom, ant1, time)
    dtr2 = CalibrationDistributor(dom, ant2, time)
    logampl = (dtr1 + dtr2).ducktape(amplkey)
    logph = 1j*(dtr1 - dtr2).ducktape(phkey)
    return (logampl + logph).exp()


def CalibrationDistributor(domain, ant, time):
    assert len(time.shape) == 1
    assert ant.shape == time.shape
    assert time.min() >= 0
    assert time.max() < domain[1].total_volume
    assert np.max(ant) < domain[0].shape[0]
    assert np.min(ant) >= 0
    dd = [ift.RGSpace(domain[0].shape, distances=1.), domain[1]]
    dd = ift.DomainTuple.make(dd)
    positions = np.array([ant, time])
    li = ift.LinearInterpolator(dd, positions)
    return li @ ift.Realizer(li.domain) @ ift.GeometryRemover(dd, 0).adjoint


@pmp('n', range(3, 40))
def test_visibility_closure_matrices(n):
    Phi = vlbi.closure.visibility_design_matrix(n)
    Psi = vlbi.closure.closure_phase_design_matrix(n)
    np.testing.assert_allclose(Psi @ Phi, 0)


@pmp('seed', seeds)
@pmp('shape', [(3, 4), (10, 20)])
def test_normalize(seed, shape):
    with ift.random.Context(seed):
        sp = ift.RGSpace(shape, distances=np.exp(ift.random.current_rng().random(len(shape))))
        s = ift.from_random(sp, 'normal').exp()
        s_normalized = vlbi.normalize(s.domain)(s)
        np.testing.assert_allclose(s_normalized.s_integrate(), 1.)


@pmp('sky', [sky_movie_mf])
def test_normalize2(sky):
    fld = sky(ift.from_random(sky.domain, 'normal'))
    if isinstance(fld, ift.Field):
        np.testing.assert_allclose(fld.s_integrate(), 1)
    else:
        val = 0
        for ff in fld.values():
            val += ff.s_integrate()
        np.testing.assert_allclose(val, 1)


@pmp('seed', seeds)
@pmp('d', ds)
@pmp('mode', ['ph', 'ampl'])
def test_closure_gradient_consistency(seed, d, mode):
    with ift.random.Context(seed):
        f = vlbi.Visibilities2ClosureAmplitudes
        if mode == 'ph':
            f = vlbi.Visibilities2ClosurePhases
        op = f(d)[0]
        pos = ift.from_random(op.domain, 'normal', dtype=np.complex128)
        ift.extra.check_jacobian_consistency(op, pos, tol=1e-7, ntries=10)


def test_saving_hdf5():
    sp1 = ift.UnstructuredDomain(2)
    sp2 = ift.UnstructuredDomain(2)
    tmp_path = 'mf_test_asehefuyiq2rgui.h5'
    mf = {'a': ift.full(sp1, 1.3), 'b': ift.full(sp2, 1.5)}
    mf = ift.MultiField.from_dict(mf)
    vlbi.save_hdf5(tmp_path, mf)
    mf2 = vlbi.load_hdf5(tmp_path, mf.domain)
    assert ((mf2 - mf)**2).s_sum() == 0
    f = ift.full(sp1, 1.3)
    vlbi.save_hdf5(tmp_path, f)
    f2 = vlbi.load_hdf5(tmp_path, f.domain)
    assert ((f2 - f)**2).s_sum() == 0
    os.system(f'rm {tmp_path}')


@pmp('seed', seeds)
@pmp('dd', ds)
@pmp('corrupt_phase', [False, True])
@pmp('corrupt_ampl', [False, True])
def test_closure_property(seed, dd, corrupt_phase, corrupt_ampl):
    with ift.random.Context(seed):
        CP = vlbi.Visibilities2ClosurePhases(dd)[0]
        CA = vlbi.Visibilities2ClosureAmplitudes(dd)[0]
        vis = ift.makeField(CP.domain, dd['vis'].copy())
        resph0 = CP(vis).val
        resam0 = CA(vis).val
        corruptedvis = dd['vis'].copy()
        nants = len(set(dd['ant1']) | set(dd['ant2']))
        for tt in np.unique(dd['time']):
            corruption = np.ones(nants)
            if corrupt_ampl:
                corruption *= np.abs(ift.random.current_rng().random(nants))
            if corrupt_phase:
                corruption = corruption*np.exp(1j*ift.random.current_rng().random(nants))
            ind = tt == dd['time']
            aa1 = dd['ant1'][ind]
            aa2 = dd['ant2'][ind]
            corruptedvis[ind] = corruptedvis[ind]*corruption[aa1]
            corruptedvis[ind] = corruptedvis[ind]*corruption[aa2].conjugate()
        vis = ift.makeField(CP.domain, corruptedvis)
        np.testing.assert_allclose(resam0, CA(vis).val)
        np.testing.assert_allclose(resph0, CP(vis).val)


@pmp('npix', [64, 98, 256])
@pmp('epsilon', [1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8])
@pmp('dd', ds)
def test_nfft(npix, epsilon, dd):
    fov = 200*vlbi.MUAS2RAD
    dom = ift.RGSpace(2*(npix,), 2*(fov/npix,))
    nfft = vlbi.RadioResponse(dom, dd['uv'], epsilon)
    ift.extra.consistency_check(nfft,
                                domain_dtype=np.float64,
                                target_dtype=np.complex128,
                                only_r_linear=True)


def test_closure():
    def vis2closure(d, ind):
        ind = np.sort(ind)
        i, j, k = ind
        vis_ind = tuple(
            np.where(np.logical_and(d['ant1'] == a, d['ant2'] == b) == 1)[0]
            for a, b in ((i, j), (j, k), (i, k)))
        phase = d['vis']/abs(d['vis'])
        return phase[vis_ind[0]]*phase[vis_ind[1]]*phase[vis_ind[2]].conjugate()

    # Antenna setup
    #  0 -- 3 -- 4
    #  | \/ |
    #  | /\ |
    #  1 -- 2
    ant1, ant2 = [], []
    for ii in range(4):
        for jj in range(ii + 1, 4):
            ant1.extend([ii])
            ant2.extend([jj])
    ant1.append(3)
    ant2.append(4)
    d = {}
    d['uv'] = ift.random.current_rng().random((len(ant1), 2))*1e7
    d['time'] = np.array([0]*len(ant1))
    d['ant1'] = np.array(ant1)
    d['ant2'] = np.array(ant2)
    randn = ift.random.current_rng().random
    d['vis'] = randn(len(ant1))*np.exp(randn(len(ant1))*2*np.pi*1j)
    d['sigma'] = abs(d['vis'])/np.sqrt(1 + np.arange(len(ant1)))

    closure = np.empty(3, dtype=np.complex128)
    closure[0] = vis2closure(d, [0, 1, 2])[0]
    closure[1] = vis2closure(d, [0, 1, 3])[0]
    closure[2] = vis2closure(d, [0, 2, 3])[0]

    _, _, closure_op = vlbi.Visibilities2ClosurePhases(d)
    vis = ift.makeField(ift.UnstructuredDomain(len(ant1)), d['vis'])
    closure_from_op = closure_op(vis).val
    np.testing.assert_allclose(closure_from_op, closure)


@pmp('d', ds)
@pmp('dt', [1/60, 1/6])
def test_calibration_dist(d, dt):
    cph = vlbi.Visibilities2ClosurePhases(d)[0]
    ca = vlbi.Visibilities2ClosureAmplitudes(d)[0]
    time = np.array(d['time']) - min(d['time'])
    npix = max(time)/dt + 1
    tspace = ift.RGSpace(npix, distances=dt)
    cal_op = AntennaBasedCalibration(tspace, time, d['ant1'], d['ant2'], 'a', 'p')
    g0 = cal_op(ift.full(cal_op.domain, 0)).val
    np.testing.assert_allclose(g0, np.ones_like(g0))
    g = cal_op(ift.from_random(cal_op.domain, 'normal'))
    vis = ift.makeField(g.domain, d['vis'])
    cal_vis = g*vis
    np.testing.assert_(np.all(vis.val != cal_vis.val))
    np.testing.assert_allclose(cph(vis).val, cph(cal_vis).val)
    np.testing.assert_allclose(ca(vis).val, ca(cal_vis).val)


@pmp('d', ds)
def test_skyscaling_invariance(d):
    cph = vlbi.Visibilities2ClosurePhases(d)[0]
    ca = vlbi.Visibilities2ClosureAmplitudes(d)[0]
    vis = ift.from_random(cph.domain, 'normal', dtype=np.complex128)
    ift.extra.assert_allclose(cph(vis), cph(0.78*vis), 0, 1e-10)
    ift.extra.assert_allclose(ca(vis), ca(0.878*vis), 0, 1e-10)


@pmp('ddtype', dtypes)
@pmp('tdtype', dtypes)
def test_DomainTupleMultiField(ddtype, tdtype):
    dom = {'lo': ift.RGSpace(5, 3), 'hi': ift.RGSpace(5, 3)}
    dom = ift.MultiDomain.make(dom)
    active_inds = ["0_lo", "4_hi"]
    foo = vlbi.DomainTuple2MultiField(dom, active_inds)
    ift.extra.consistency_check(foo, domain_dtype=ddtype, target_dtype=tdtype)


@pmp('seed', seeds)
def test_random_states(seed):
    vlbi.save_random_state('pref')
    arr0 = ift.random.current_rng().random(5)
    ift.random.push_sseq_from_seed(seed)
    arr1 = ift.random.current_rng().random(5)
    vlbi.load_random_state('pref')
    arr2 = ift.random.current_rng().random(5)
    np.testing.assert_equal(arr0, arr2)
    with pytest.raises(AssertionError):
        np.testing.assert_equal(arr0, arr1)


@pmp('op', [ift.GeometryRemover(dom), ift.GeometryRemover(dom).ducktape('aa')])
@pmp('pre', ['', 'foo'])
@pmp('iteration', [0, 32])
def test_position_states(op, pre, iteration):
    pos0 = ift.from_random(op.domain, 'normal')
    samples0 = [ift.from_random(op.domain, 'normal') for _ in range(2)]
    vlbi.save_state(op, pos0, pre, iteration, samples0)
    pos2 = ift.from_random(op.domain, 'normal')
    pos1, samples1, current_iter = vlbi.load_state(op.domain, pre)
    assert iteration == current_iter
    ift.extra.assert_allclose(pos2, ift.from_random(op.domain, 'normal'), 0, 1e-7)
    ift.extra.assert_allclose(pos0, pos1, 0, 1e-7)
    for ss0, ss1 in zip(samples0, samples1):
        ift.extra.assert_allclose(ss0, ss1, 0, 1e-7)


def _std_check(arr):
    tol = np.std(arr) if arr.size > 3 else 0.5
    np.testing.assert_allclose(np.mean(arr), 1, atol=tol)


def _cov_check(mat):
    mat = np.copy(mat)
    for i in range(mat.shape[0]):
        mat[i] = np.roll(mat[i], -i)
    mm = np.zeros(mat.shape[1])
    mm[0] = 1.
    tol = np.std(mat) if mat.shape[0] > 3 else 0.5
    np.testing.assert_allclose(np.mean(mat, axis=0), mm, atol=tol)


def _make_vis_N(dom, d):
    vis = ift.makeField(dom, d['vis'])
    # Assume that the sigma in the data describes the noise variation for real
    # and imaginary part individually.
    N = ift.makeOp(ift.makeField(dom, d['sigma']**2))
    return vis, N


@pmp('d', ds)
def test_closure_noise(d):
    nsamples = 100

    # Phases
    vis2clos = vlbi.Visibilities2ClosurePhases(d)[0]
    vis, N = _make_vis_N(vis2clos.domain, d)
    sc = []
    for _ in range(nsamples):
        n = N.draw_sample_with_dtype(np.complex128)
        clos = vis2clos(vis + n)
        sc.append(clos.val)
    sc = np.array(sc)
    scm = np.mean(sc, axis=0)
    # np.std adds var of imag and real part
    # however the Gauss is not really 2D
    val = np.std(sc, axis=0) 
    mat = 0.
    for s in sc:
        tm = s-scm
        mat += np.outer(tm, tm.conjugate())
    # the line before adds var of imag and real part
    # however the Gauss is not really 2D
    mat /= nsamples 

    _std_check(val)
    _cov_check(mat)

    # Amplitudes
    vis2clos = vlbi.Visibilities2ClosureAmplitudes(d)[0]
    vis, N = _make_vis_N(vis2clos.domain, d)
    sc = []
    for _ in range(nsamples):
        n = N.draw_sample_with_dtype(np.complex128)
        clos = vis2clos(vis + n)
        sc.append(clos.val)
    sc = np.array(sc)
    scm = np.mean(sc, axis=0)
    val = np.std(sc, axis=0)
    mat = 0.
    for s in sc:
        tm = s-scm
        mat += np.outer(tm, tm)
    mat /= nsamples
    _std_check(val)
    _cov_check(mat)
