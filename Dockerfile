FROM python:3.6-buster

RUN pip3 install "scipy>=1.4" h5py matplotlib pytest pytest-cov
RUN pip3 install git+https://gitlab.mpcdf.mpg.de/ift/nifty@NIFTy_6 git+https://gitlab.mpcdf.mpg.de/ift/nifty_gridder.git@f1540b9cca161e1aa32aa3cd09a592b3dac5f16b 

ENV MPLBACKEND agg
WORKDIR /eht
