# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2021 Max-Planck-Society
# Author: Philipp Arras, Philipp Frank, Philipp Haim, Reimar Leike,
# Jakob Knollmueller

import numpy as np

import nifty6 as ift
import src as vlbi

# MPI information
comm, nranks, rank, master = ift.utilities.get_MPI_params()


np.seterr(all='raise', under='warn')
if master:
    print('Numpy mode:', np.geterr())

oversampling_factor = 2
nthreads = 1
ift.fft.set_nthreads(nthreads)
eps = 1e-7
radius_fitting_init = {'m87': [5, 0, 1, 15],  # cx, cy, step, dist
                       'crescent': [10, -25, 1, 15],
                       'ehtcrescent': [0, 0, 1, 15]}
radius_fitting_dtheta, radius_fitting_sample_r = 1, 0.5*vlbi.MUAS2RAD

# vlbi.set_SNR_cutoff(0.2)

npix = 256
dt = 6  # hours
# npix = 128
# dt = 24  # hours
startt = 0
npixt = 7*24//dt
fov = 256*vlbi.MUAS2RAD
min_timestamps_per_bin = 12  # how many different time stamps should be at least in a time bin
npix = int(npix)
doms = ift.RGSpace(2*(npix,), 2*(fov/npix,))

zm = 0, 0.2, 0.1, ''
spaceparams = {
    'target_subdomain': doms,
    'fluctuations_mean':   1.5,
    'fluctuations_stddev': 1.,
    'loglogavgslope_mean': -1.5,
    'loglogavgslope_stddev': 0.5,
    'flexibility_mean':   0.01,
    'flexibility_stddev': 0.001,
    'asperity_mean':   0.0001,
    'asperity_stddev': 0.00001,
    'prefix': 'space'
}
timeparams = {
    'fluctuations_mean':   0.2,
    'fluctuations_stddev': 1.,
    'loglogavgslope_mean': -4,
    'loglogavgslope_stddev': 0.5,
    'flexibility_mean':   0.01,
    'flexibility_stddev': 0.001,
    'asperity_mean':   0.001,
    'asperity_stddev': 0.0001,
    'prefix': 'time'
}


domt = ift.RGSpace(npixt, dt)
dom = ift.makeDomain((domt, doms))
domt_zeropadded = ift.RGSpace(int(oversampling_factor*npixt), dt)

cfm = ift.CorrelatedFieldMaker.make(*zm)
cfm.add_fluctuations(domt_zeropadded, **timeparams)
cfm.add_fluctuations(**spaceparams)
logsky = cfm.finalize(prior_info=0)

FA_lo = ift.FieldAdapter(logsky.target, 'lo')
FA_hi = ift.FieldAdapter(logsky.target, 'hi')
xi_hi = ift.FieldAdapter(logsky.domain['xi'], 'xi_hi')
id_hi = ift.FieldAdapter(logsky.domain['xi'], 'xi').adjoint @ xi_hi
xi_lo = ift.FieldAdapter(logsky.domain['xi'], 'xi_lo')
id_lo = ift.FieldAdapter(logsky.domain['xi'], 'xi').adjoint @ xi_lo

expected_difference = 1e-2
logsky_1 = (FA_hi.adjoint @ logsky).partial_insert(id_hi)
logsky_2 = (FA_lo.adjoint @ logsky).partial_insert(id_lo).scale(expected_difference)
logsky_lo = FA_lo.adjoint @ (FA_hi + FA_lo)
logsky_hi = FA_hi.adjoint @ (FA_hi - FA_lo)
logsky_mf = (logsky_hi + logsky_lo) @ (logsky_1 + logsky_2)

sky_movie_mf = vlbi.normalize(logsky_mf.target) @ logsky_mf.exp()
pspec_movie = cfm.normalized_amplitudes
