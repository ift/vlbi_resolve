.PHONY: m87 blobs disk crescent ehtcrescent ehtdisk sim1 sim2

pre := MPLBACKEND=agg MKL_NUM_THREADS=1 OMP_NUM_THREADS=1

ifdef VLBI_MPI_TASKS
python = $(pre) mpirun -np $(VLBI_MPI_TASKS) python3
else
python = $(pre) python3
endif

initial.h5 :
	$(python) movie_start.py

m87 : initial.h5
	$(python) reconstruction.py m87 initial.h5

blobs : initial.h5
	$(python) reconstruction.py blobs initial.h5

disk : initial.h5
	$(python) reconstruction.py disk initial.h5

crescent : initial.h5
	$(python) reconstruction.py crescent initial.h5

ehtcrescent : initial.h5
	$(python) reconstruction.py ehtcrescent initial.h5

ehtdisk : initial.h5
	$(python) reconstruction.py ehtdisk initial.h5

sim1 : initial.h5
	$(python) reconstruction.py sim1 initial.h5

sim2 : initial.h5
	$(python) reconstruction.py sim2 initial.h5
