# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2021 Max-Planck-Society
# Author: Philipp Arras, Martin Reinecke

import nifty_gridder as ng
import numpy as np

import nifty6 as ift

from .constants import SPEEDOFLIGHT


class RadioResponse(ift.LinearOperator):
    def __init__(self, domain, uv, epsilon, j=1):
        ndata = uv.shape[0]
        self._uvw = np.zeros((ndata, 3), dtype=uv.dtype)
        self._uvw[:, 0:2] = uv
        self._j = int(j)
        self._eps = float(epsilon)
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(ift.UnstructuredDomain(ndata))
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        x = x.val
        dx, dy = self._domain[0].distances
        nx, ny = self._domain[0].shape
        f = np.array([SPEEDOFLIGHT])
        if mode == self.TIMES:
            res = ng.dirty2ms(self._uvw, f, x, None, dx, dy, self._eps, False,
                              nthreads=1)
            res = res[:, 0]
        else:
            x = x[:, None]
            res = ng.ms2dirty(self._uvw, f, x, None, nx, ny, dx, dy, self._eps,
                              False, nthreads=1)
        res *= self._domain[0].scalar_dvol
        return ift.makeField(self._tgt(mode), res)
