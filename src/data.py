# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2021 Max-Planck-Society
# Author: Philipp Arras, Philipp Frank, Philipp Haim, Reimar Leike,
# Jakob Knollmueller

import csv
from collections import defaultdict

import numpy as np
from scipy.sparse import coo_matrix
from scipy.sparse.linalg import aslinearoperator

from .constants import MAXSHORTBASELINE, KEYS, FREQS, DAYS


VIS_SNR_THRESHOLD = 1
REL_SYS_NOISE = 0.01


def _short_baseline_indices(uv):
    return np.linalg.norm(uv, axis=1) < MAXSHORTBASELINE


def read_data_raw(fname):
    ant1, ant2, time = [], [], []
    uv, vis = [], []
    sigma = []
    print(f'Load {fname}')
    with open(fname, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        row = next(reader)
        freq = float(row[2].split(':')[1][:-3])*1e9
        for row in reader:
            if row[0][0] == '#':
                continue
            time.append(float(row[0]))
            ant1.append(row[1])
            ant2.append(row[2])
            uv.append((float(row[3]), float(row[4])))
            vis.append(float(row[5])*np.exp(1j*float(row[6])/180*np.pi))
            sigma.append(float(row[7]))
    uv = np.array(uv)
    uv[:, 1] *= -1
    return {
        'time': np.array(time),
        'ant1': np.array(ant1),
        'ant2': np.array(ant2),
        'uv': np.array(uv),
        'vis': np.array(vis),
        'sigma': np.array(sigma),
        'freq': freq,
    }


def read_data(prefix, day, freq, ts_per_bin):
    assert freq in FREQS
    assert day in DAYS
    fname = f'{prefix}_{day}_{freq}.csv'
    d = read_data_raw(fname)

    # add_systematic_noise_budget(d)
    # remove_low_snr(d)

    ant1 = d['ant1']
    ant2 = d['ant2']
    ddct = defaultdict(lambda: len(ddct))
    d['ant1'] = np.array([ddct[ele] for ele in ant1])
    d['ant2'] = np.array([ddct[ele] for ele in ant2])

    # Combine over scan
    if ts_per_bin > 1:
        tbins = []
        times = d['time']
        nval = len(d['time'])
        i0 = 0

        def fair_share(n, nshare, ishare):
            return n//nshare + (ishare < (n % nshare))

        while i0 < nval:
            i = i0+1
            nscan = 1  # number of different time stamps in the scan
            while i < nval and times[i]-times[i-1] < 20./3600:  # as long as there are less than 20s between time stamps, we assume we are in the same scan
                if times[i] != times[i-1]:
                    nscan += 1
                i += 1
            nbin = max(1, nscan//ts_per_bin)  # how many bins to use for this scan
            for j in range(nbin):
                n = fair_share(nscan, nbin, j)
                i = i0+1
                icnt = 0
                oldtime = times[i0]
                while i < nval and icnt < n:
                    if times[i] != oldtime:
                        icnt += 1
                        oldtime = times[i]
                    if icnt < n:
                        if icnt == n-1:
                            tbins += [(times[i0],
                                times[i],
                                times[i]-times[i0])]
                        times[i] = times[i0]  # give all values in this bin the time stamp of the first value
                        i += 1
                i0 = i
        tbsize = np.array([t[2] for t in tbins])
    else:
        print('No temporal averaging.')
    # End combine over scan

    data_ordering(d)
    remove_short_baselines(d)

    identifier = []
    for i in range(len(d['time'])):
        identifier += [(d['time'][i], d['ant1'][i], d['ant2'][i])]
    # Add dummy element so that it does not get cast to a 2D numpy array
    identifier += [(-1e29,)]
    a = np.argsort(identifier)
    to_combine = []
    current_list = [a[1]]
    for i in range(2, a.shape[0]):
        if identifier[a[i]] == identifier[current_list[-1]]:
            current_list += [a[i]]
        else:
            to_combine += [current_list]
            current_list = [a[i]]
    to_combine += [current_list]
    d = combine_baselines(d, to_combine)

    sanity_checks(d)
    return d


def combine_baselines(d, to_combine):
    for l in to_combine:
        l.sort()
    to_combine.sort()
    N = d['sigma']**2
    cols = []
    rows = []
    assumed_N = []
    actual_std = []
    for i, l in enumerate(to_combine):
        cols += [i]*len(l)
        rows += l
        assumed_N += [np.mean(N[l])]
        actual_std += [np.std(d['vis'][l])]
    R = aslinearoperator(
        coo_matrix((np.ones_like(rows), (rows, cols)),
                   (to_combine[-1][-1] + 1, len(to_combine))))
    D = 1/R.rmatvec(1/N)
    assumed_N = np.array(assumed_N)
    actual_std = np.array(actual_std)
    quot = actual_std/np.sqrt(assumed_N)
    with open("time_averaging.txt", 'a') as f:
        f.write("{} {} {} {}\n".format(np.amin(quot), np.amax(quot), np.mean(quot), np.median(quot)))
    print("min max avg med")
    print("{} {} {} {}".format(np.amin(quot), np.amax(quot), np.mean(quot), np.median(quot)))
    m = D*(R.rmatvec(d['vis']/N))

    res = {}
    for kk in d:
        if kk == 'freq':
            res[kk] = d[kk]
        elif kk == 'sigma':
            res[kk] = np.sqrt(D)
        elif kk == 'vis':
            res[kk] = m
        else:
            res[kk] = d[kk][[l[0] for l in to_combine]]
    sanity_checks(res)
    return res


def remove_short_baselines(d):
    uvlen = np.linalg.norm(d['uv'], axis=1)
    ind = np.logical_and(uvlen >= MAXSHORTBASELINE, d['ant1'] != d['ant2'])
    for kk in d:
        if kk == 'freq':
            continue
        d[kk] = d[kk][ind]


def remove_low_snr(d):
    ind = np.abs(d['vis'])/d['sigma'] > VIS_SNR_THRESHOLD
    for kk in d:
        if kk == 'freq':
            continue
        d[kk] = d[kk][ind]


def add_systematic_noise_budget(d):
    d['sigma'] = np.sqrt((abs(d['vis'])*REL_SYS_NOISE)**2 + d['sigma']**2)


def data_ordering(d):
    """Swap ant1, ant2 such that ant1 < ant2, sort after time, ant1, ant2"""
    # FIXME Check: Every baseline only once per timestamp?
    foo = d['ant1'] > d['ant2']
    tmp = d['ant1'][foo]
    d['ant1'][foo] = d['ant2'][foo]
    d['ant2'][foo] = tmp
    d['vis'][foo] = d['vis'][foo].conjugate()
    d['uv'][foo] *= -1
    foo = np.lexsort((d['ant2'], d['ant1'], d['time']))
    for kk in KEYS:
        d[kk] = d[kk][foo]


def combined_data(prefix, freqs, ts_per_bin):
    d = {kk: [] for kk in KEYS}
    time_offsets = [0, 1, 5, 6]
    for ii, day in enumerate(DAYS):
        time_offset = time_offsets[ii]*24.
        for freq in freqs:
            dd = read_data(prefix, day, freq, ts_per_bin)
            dd['time'] += time_offset
            if freq == 'lo':
                dd['time'] += 1e-6
            for kk in KEYS:
                d[kk].append(dd[kk])
    for kk in KEYS:
        d[kk] = np.concatenate(d[kk], axis=0)
    data_ordering(d)
    sanity_checks(d)
    return d


def time_binning(d, delta_t, tmin, tmax):
    d_bin = []
    t_current = tmin
    for i in range((tmax - tmin)//delta_t):
        ind = np.logical_and(d['time'] < t_current + delta_t,
                             d['time'] >= t_current)
        t_current += delta_t
        d_aux = {}
        if np.sum(ind) == 0:
            d_bin += [d_aux]
            continue
        for key in KEYS:
            d_aux[key] = d[key][ind]
        d_bin += [d_aux]
    return d_bin


def sanity_checks(d):
    assert np.all(d['ant1'] < d['ant2'])
    assert np.all(np.diff(d['time']) >= 0)
    assert np.all(d['sigma'] > 0)
    assert np.all(np.diff(np.lexsort((d['ant2'], d['ant1'], d['time']))) == 1)
    assert np.iscomplexobj(d['vis'])
    assert np.isrealobj(d['uv'])
    l0 = d['ant1'].shape[0]
    assert np.all(
        np.unique(np.vstack([d['time'], d['ant1'], d['ant2']]),
                  return_counts=True,
                  axis=1)[1] == 1)
    foo = np.lexsort((d['ant2'], d['ant1'], d['time']))
    np.testing.assert_allclose(np.diff(foo), 1)
    for kk, vv in d.items():
        if kk in ['antname2id', 'freq']:
            continue
        if kk == 'uv':
            assert len(vv.shape) == 2
        else:
            assert len(vv.shape) == 1
        assert vv.shape[0] == l0
