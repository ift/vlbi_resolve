# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2021 Max-Planck-Society

import numpy as np

DEG2RAD = np.pi/180.
SPEEDOFLIGHT = 299792458.

ARCMIN2RAD = 1/60*DEG2RAD
AS2RAD = 1/3600*DEG2RAD
MUAS2RAD = 1/1e6/3600*DEG2RAD

MAXSHORTBASELINE = 2000000
KEYS = ['time', 'ant1', 'ant2', 'uv', 'vis', 'sigma']
FREQS = ['lo', 'hi']
DAYS = ['095', '096', '100', '101']
_inches_per_pt = 1/72.27
TEXTWIDTH = 455.24417*_inches_per_pt
