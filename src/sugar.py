# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019-2021 Max-Planck-Society
# Author: Philipp Arras, Philipp Frank, Philipp Haim, Reimar Leike,
# Jakob Knollmueller

import h5py as h5
import numpy as np

import nifty6 as ift


def normalize(domain):
    domain = ift.makeDomain(domain)
    if isinstance(domain, ift.DomainTuple):
        fac = domain.scalar_weight()
        vdot = ift.ContractionOperator(domain, spaces=None)
    else:
        assert domain['hi'] is domain['lo']
        fac = domain['hi'].scalar_weight()
        vdot_hi = ift.ContractionOperator(domain['hi'], spaces=None)
        vdot_lo = ift.ContractionOperator(domain['lo'], spaces=None)
        vdot = vdot_hi.ducktape('hi') + vdot_lo.ducktape('lo')
    return ift.ScalingOperator(domain, 1)*(vdot.adjoint @ vdot.ptw('reciprocal').scale(1/fac))


def save_hdf5(path, field):
    print(f'Save {path}')
    if isinstance(field, ift.Field):
        field = ift.MultiField.from_dict({'single_field': field})
    fh = h5.File(path, 'w')
    for key, f in field.items():
        fh.create_dataset(key, data=f.val, dtype=f.dtype)
    fh.close()


def save_hdf5_iterable(path, iterable, master=True):
    if master:
        print(f'Save {path}')
        fh = h5.File(path, 'w')
    # CAUTION: the loop and the if-clause cannot be interchanged!
    for kk, field in enumerate(iterable):
        if master:
            grp = fh.create_group(str(kk))
            if isinstance(field, ift.Field):
                field = ift.MultiField.from_dict({'single_field': field})
            for key, f in field.items():
                vv = f.val if isinstance(f, ift.Field) else f
                grp.create_dataset(key, data=vv, dtype=f.dtype)
    if master:
        fh.close()


def save_state(sky, pos, pre, current_iter, samples=None, master=True):
    if master:
        save_random_state(pre)  # FIXME
        save_hdf5(f'{pre}position.h5', pos)
    if samples is not None:
        save_hdf5_iterable(f'{pre}samples.h5', samples, master)
    if master:
        np.savetxt(f'{pre}current_iteration.txt', np.array([int(current_iter)]),
                   fmt='%i')


# FIXME: not MPI-compatible yet
def load_state(domain, pre):
    load_random_state(pre)  # FIXME
    pos = load_hdf5(f'{pre}position.h5', domain)
    samples = load_hdf5_lst(f'{pre}samples.h5', domain)
    current_iter = np.loadtxt(f'{pre}current_iteration.txt')[()]
    return pos, samples, current_iter


def save_random_state(pre):
    with open(f'{pre}random.pickle', 'wb') as f:
        f.write(ift.random.getState())


def load_random_state(pre):
    with open(f'{pre}random.pickle', 'rb') as f:
        ift.random.setState(f.read())


def load_hdf5(path, domain=None):
    print(f'Load {path} ', end='', flush=True)
    fh = h5.File(path, 'r')
    if len(fh) == 1 and 'single_field' in fh.keys():
        res = np.array(fh['single_field'])
    else:
        res = {kk: np.array(vv) for kk, vv in fh.items()}
    fh.close()
    if domain is None:
        return res
    print('ok')
    return ift.makeField(domain, res)


def load_hdf5_lst(path, domain=None, index=None):
    ss = 'all' if index is None else index
    print(f'Load {path} {ss} ', end='', flush=True)
    fh = h5.File(path, 'r')
    res = []
    for kk in fh:
        if index is not None and int(kk) != index:
            continue
        obj = fh[kk]
        if len(obj) == 1 and 'single_field' in obj.keys():
            res.append(np.array(obj['single_field']))
        else:
            res.append({kk: np.array(vv) for kk, vv in obj.items()})
    fh.close()
    print('ok')
    if domain is not None:
        res = [ift.makeField(domain, rr) for rr in res]
    if index is None:
        return res
    assert len(res) == 1
    return res[0]


def len_hdf5_lst(path):
    print(f'Open {path} ', end='', flush=True)
    with h5.File(path, 'r') as f:
        n = len(f)
    print('ok')
    return n


def baselines(alst):
    return [(aa, bb) for ii, aa in enumerate(alst) for bb in alst[ii + 1:]]


def binom2(n):
    return (n*(n-1))//2


def binom3(n):
    return (n*(n-1)*(n-2))//6


def binom4(n):
    return (n*(n-1)*(n-2)*(n-3))//24


def sigmoid(x):
    return .5*(1 + np.tanh(x))


def gaussian_profile(dom, rad):
    dom = ift.makeDomain(dom)[0]
    xf, xp = dom.distances[0]*dom.shape[0]/2, dom.shape[0]
    yf, yp = dom.distances[1]*dom.shape[1]/2, dom.shape[1]
    xx, yy = np.meshgrid(np.linspace(-xf, xf, xp),
                         np.linspace(-yf, yf, yp),
                         indexing='ij')
    profile = 1/(2*np.pi*rad**2)*np.exp(-0.5*(xx**2 + yy**2)/rad**2)
    return ift.makeField(dom, profile)


class DomainTuple2MultiField(ift.LinearOperator):
    def __init__(self, domain, active_inds):
        self._domain = domain
        tgt = {
            str(ii): self._domain[domain.keys()[0]][1:]
            for ii in set(active_inds)
        }
        self._target = ift.makeDomain(tgt)
        self._capability = self.TIMES | self.ADJOINT_TIMES
        self.dom0 = self._domain[self._domain.keys()[0]]

    def apply(self, x, mode):
        self._check_input(x, mode)
        x = x.val
        if mode == self.TIMES:
            return ift.MultiField.from_dict({
                ii: ift.makeField(self.dom0[1:], x[ii[-2:]][int(ii[:-3])])
                for ii in self._target.keys()
            })
        res = {}
        for f in ['lo', 'hi']:
            res[f] = np.zeros(self.dom0.shape,
                              dtype=x[list(self._target.keys())[0]].dtype)
        for ii in self._target.keys():
            res[ii[-2:]][int(ii[:-3])] = x[ii]
        res = {k: ift.Field.from_raw(self.dom0, v) for k, v in res.items()}
        return ift.MultiField.from_dict(res)


def freq_avg(fld):
    return 0.5*(fld['hi'] + fld['lo'])


def strip_oversampling(res, ofac):
    res = res.to_dict()
    for kk, vv in res.items():
        n = int(np.round(vv.shape[0]/ofac))
        domt = ift.RGSpace(n, vv.domain[0].distances[0])
        dom = domt, vv.domain[1]
        res[kk] = ift.makeField(dom, vv.val[:n])
    return ift.MultiField.from_dict(res)
