# VLBI Resolve: Dynamic VLBI Imaging with information field theory

This repository belongs to the paper "The variable shadow of M87*" (https://arxiv.org/abs/2002.05218).

# Installation

Make sure that you have a python version >= 3.6. Then install the python packages scipy, matplotlib, nifty and nifty_gridder.

```
pip3 install --user "scipy>=1.4" matplotlib git+https://gitlab.mpcdf.mpg.de/ift/nifty@3f31d6b308387400cdfd7cd8c36e74f80b7a46cb git+https://gitlab.mpcdf.mpg.de/ift/nifty_gridder.git@f1540b9cca161e1aa32aa3cd09a592b3dac5f16b
```

# Validation examples

There are a total of 6 validation examples for which mock data can be found in `data/`. These examples have the internal names

```
blobs, crescent, disk, ehtcrescent, sim1, sim2
```

and the `.csv` files have the same structure as the M87* data.

# M87* data

The data obtained by the Event-Horizon-Telescope (EHT) about M87* can be found [here](https://doi.org/10.25739/g85n-f134). To use this data in our pipeline, the `.csv` files have to be renamed to match the naming convention of the examples. For example the `.csv` file `SR1_M87_2017_095_hi_hops_netcal_StokesI.csv` has to be renamed to `m87_095_hi.csv`.

# Reconstructions

The reconstructions can be computed using `make`. For example the results for `blobs` can be generated using the command

```
make blobs
```

MPI support
-----------

The reconstruction files support MPI parallelization. It can be enabled using the flag `VLBI_MPI_TASKS`. Specifically the command

```
VLBI_MPI_TASKS=10 make m87
```

runs the reconstruction for the `m87` data using 10 MPI tasks.



# Tests

In order to run the tests you need pytest. Then run:

```
pytest-3 test.py
```
